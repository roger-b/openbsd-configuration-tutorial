# OpenBSD 

A guide to how I setup OpenBSD as my desktop. Just a compilation of eveything I have found interesting on the interwebs, in the awesome OpenBSD documentation and my own implementations, just so that I don't have to remember everything.  


## About my System 

![scrot](https://gitlab.com/rxdgr/openbsd-configuration-tutorial/uploads/22c3274fc17b105dff1649a8acd1a1d2/scrot.png)  

## Software I run 

- neofetch
- feh
- git
- suckless (dwm / st /dmenu)
- picom
- vim
- clang
- keepassxc
- gnupg
- firefox-esr 
- scrot 
- xpdf


### Installation 

very easy steps, fren.  

You will be prompted for some basic information. Simple questions to set up your system. No big deal here. 
Make sure you choose the right disk for installation. I disable xenodm bloat. <code>startx</code> does the job perfectly fine. 

### Partitioning

Before deciding on a layout for my file system, I install and use a single partition, for a few days / weeks, and then with the <code>du</code> command, determine what my hierarchy is actually consuming. From there, I decide what custom layout best fits my needs. This procedure is also recommended in the OpenBSD documentation. 

This is how I have thus organized my file system : 

1. 
2. 
3. 
4. 
5. 

## Post Install - First Boot 

If you didn't previously create a user, you should do it now. 

For root, type <code>su</code> and your password, then run `syspatch`. 

Now run : 

    pkg_add neofetch git vim

### Adding user to the wheel 

    vim /etc/doas.conf 
    permit persist user as root

### Setting up $HOME as your GIT repo

This is a great tip I got from durdn @ Atlassian. It requires no messing around with symlinks and allows for easy reinstall on a new system. I also have a script for his commands : 

I use :  

    git init --bare $HOME/.myconf
    alias config='/usr/local/bin --git-dir=$HOME/.myconf/ --work-tree=$HOME'
    config config status.showUntrackedFiles no  

`alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'` to my shell dotfile to make the alias permanent.  
Do not forget to invoke it by adding `export ENV="$HOME/.kshrc` to /$HOME/.profile


### Managing Dotfiles 

Assuming you have configured git to your needs, you can now version whatever files you want using the normal commands :  

    config status
    config add .vimrc
    config commit -m "Add vimrc"
    config remote add remote_name https://gitlab.com/cameronmcnz/example-website.git   # use if remote repo is not yet added
    config push -u -f remote_name branch_name 


## System Settings 

### Battery Life 

Since I am using a laptop, it is important to enable power management. 

    rcctl enable apmd
    rcctl set apmd flags -A
    rcctl start apmd

### Wi-Fi  

No need to deal with Linux `wpa_supplicant` nightmare, how smooth is this ?

check network interfaces with `ifconfig` then : 

    ifconfig interface_name up
    ifconfig interface_name scan

You can know edit /etc/hostname.interface_name for persistence :

    join "YOUR_SSID" wpakey "YOUR_PASSPHRASE"
    dhcp
    inet6 autoconf
    up powersave

To test your changes, reset the interfaces and run the netstart script :

    ifconfig em0 down
    ifconfig iwn0 down
    pkill dhclient
    sh /etc/netstart

This should automatically tighten up permissions on the hostname.if file and join your network.

## dwm / dmenu / st 

    mkdir suckless/dwm/
    cd suckless/dwm/
    git clone https://git.suckless.org/dwm
    vim config.mk #uncomment the openBSD part 
    make  
    make clean install
    exec /usr/local/bin/dwm at the end of .xinitrc
    startx to lauch dwm

same steps as above for both dmenu and st

always remove config.h !

to remove the little bit of space on the bottom and right side, in config.def.h

    static const int resizehints = 0;

this is how to patch your diff files :  

    patch < file.diff 

